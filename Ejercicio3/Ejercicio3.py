class alumno:

    def __init__(self, nombre, clase):
        self.nombre = nombre
        self.clase = clase

    def __str__(self):
        return "El alumno {} pertenece a la clase {}".format(self.nombre, self.clase)

    def cambiarDeClase(self, nuevaClase):
        self.clase = nuevaClase

    def guardarAlumno(self):
        archivo = open("Datos.txt", "a")
        archivo.write(self.__str__()+"\n")
        archivo.close()

def compararAlumno(alumnno1, alumno2):
    if (alumnno1.clase == alumno2.clase):
        print("Son de la misma clase")
    else:
        print("No pertenencen a la misma clase")

def CrearFichero():
    global archivo
    archivo = open("Datos.txt", "w")
    archivo.close()

