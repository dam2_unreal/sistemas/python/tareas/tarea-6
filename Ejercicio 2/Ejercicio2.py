#-*- coding: utf-8 -*-
#!/usr/bin/env python
# Este archivo usa el encoding: utf-8
#-*- coding: Windows-1252 -*-
#-*- coding: cp850 -*-
#-*- coding: cp1252 -*-
#-*- coding: IBM850 -*-

import os

Cuentas=[]
Monto=[]
rep=0


def Menu():
    os.system('pause')
    os.system('cls')
    print("Bienvenido al Banco X")
    print("Elija alguna de las siguientes opciones para continuar \n")
    print("1.- Registrarse en nuestro Banco")
    print("2.- Consultar su saldo")
    print("3.- Calcular interes que generara su saldo a cierto tiempo")
    print("4.- Si quiere salir")


def RegistroEnElBanco():
    global nom
    print("HA ELIGIDO EL REGISTRO DE CLIENTE \n")
    nom = input("Ingrese su nombre:\n")
    Cuentas.append(nom)
    try:
        mon = float(input("Ingrese la cantidad que desea depositar a su cuenta:\n"))
    except Exception:
        print("Error al introducir datos, se te pondra un valor por defecto")
        mon = float(0)
    Monto.append(mon)


def Saldo(nom):
    global ind
    if (nom in Cuentas):
        ind = Cuentas.index(nom)
        saldo = Monto[ind]
        print("Su saldo es: ", saldo, "€ ")
        print("")
    else:
        print("")
        print("Ese nombre no existe")
        print("")


def InteresQueGenerara(nom):
    global ind
    if (nom in Cuentas):
        ind = Cuentas.index(nom)
        p = Monto[ind]
        try:
            n = int(input("Ingrese el numero de años que va a invertir su dinero\n"))
        except Exception:
            print("Error al introducir datos, se te pondra un valor por defecto")
            n = int(10)
        r = 0.05
        c = p * (1 + r) * n
        round(c)
        print("")
        print('El interes generado en ', n, ' a'u'ños es de $', c)
        print("")
    else:
        print("")
        print("Ese nombre no existe")
        print("")


while(rep==0):
    Menu()
    try:
        o = int(input(" Elige (1-4):"))
    except Exception:
        o = 5

    if (o == 1):
        RegistroEnElBanco()
    elif(o == 2):
        print("HA ELIDO CONSULTAR SU SALDO\n")
        nom=input("Ingrese su nombre:\n")
        Saldo(nom)
    elif(o==3):
        print("HA ELEGIDO CALCULAR EL INTERES QUE GERARA SU CUENTA\n")
        nom=input("Ingrese su nombre: \n")
        InteresQueGenerara(nom)
    elif(o==4):
        exit()
    elif(o<1 or o>4):
        print("")
        print('Esa no es una opción intente de nuevo\n')
        print("")