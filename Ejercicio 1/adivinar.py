from random import *

def generaNumerosAleatorios(min,max):
    try:
        if min>max:
            aux = min
            min = max
            max = aux

        return randint(min, max)
    except TypeError:
        print("Debes escribir numeros")
        return -1

def adivinarNumero(num):
    if(num == numSecret):
        print("Felicidades, has adivinado el numero secreto,",numSecret,", en solo ",intentos," intentos")
        return False
    elif(num>numSecret):
        print("El numero es menor")
        return True
    else:
        print("El numero es mayor")
        return True

intentos = 0
numSecret = generaNumerosAleatorios(1,100)
victoria = True

print("Adivina el numero secreto entre 1 y 100: ")
while(victoria):
    intentos += 1
    victoria = adivinarNumero(int(input("Escibe un numero: ")))

